/*jslint browser this */
/*global _, shipFactory, player, utils */

(function (global) {
    "use strict";

    var ship = {dom: {parentNode: {removeChild: function () {}}}};
    
    var player = {
        grid: [],
        tries: [],
        fleet: [],
        currentPlayer: 0,
        touchedBoat: 0,
        gameOver: 0,
        game: null,
        activeShip: 0,
        init: function () {
            // créé la flotte
            this.fleet.push(shipFactory.build(shipFactory.TYPE_BATTLESHIP));
            this.fleet.push(shipFactory.build(shipFactory.TYPE_DESTROYER));
            this.fleet.push(shipFactory.build(shipFactory.TYPE_SUBMARINE));
            this.fleet.push(shipFactory.build(shipFactory.TYPE_SMALL_SHIP));
            
            // créé les grilles
            this.grid = utils.createGrid(10, 10);
            this.tries = utils.createGrid(10, 10);
        },
        setGame: function (game){
            this.game = game;
        },
        play: function (col, line) {
            // appel la fonction fire du game, et lui passe une calback pour récupérer le résultat du tir
            this.game.fire(this, col, line, _.bind(function (hasSucced) {
                this.tries[line][col] = hasSucced;
            }, this));
        },
        // quand il est attaqué le joueur doit dire si il a un bateaux ou non à l'emplacement choisi par l'adversaire
        receiveAttack: function (col, line, callback) {
            var succeed = false;
            if (this.grid[line][col] !== 0) {
                succeed = true;
                if (this.grid[line][col] !== 0 && this.grid[line][col] !== 10) {
                    this.touchedBoat = this.grid[line][col];
                }
                this.grid[line][col] = 10;
            }
            callback.call(undefined, succeed);
        },
        ConfirmShipPosition: function(posX, posY) {
            var shipName = this.fleet[this.activeShip].name
            if(this.game.stateOrientation==="horizontal"){ 
                if(shipName === "Battleship" || shipName === "Destroyer") {
                    if((posX - 2) >= 0 && (posX +2) < this.grid.length)
                    return true                    
                }
                else if (shipName === "Submarine") {
                    if((posX - 2) >= 0 && (posX +1) < this.grid.length)
                        return true
                }
                else {
                    if((posX - 1) >= 0 && (posX +1) < this.grid.length)
                        return true
                }
            }
            else {
                if(shipName === "Battleship" || shipName === "Destroyer") {
                    if((posY - 2) >= 0 && (posY +2) < this.grid.length)
                        return true                    
                }
                else if ( shipName === "Submarine") {
                    if((posY - 2) >= 0 && (posY +1) < this.grid.length)
                        return true
                }
                else {
                    if((posY - 1) >= 0 && (posY +1) < this.grid.length)
                        return true
                }
            }
        },
        CheckEmptyCase: function(posX,posY) {
            var shipLength = this.fleet[this.activeShip].life
            var ship = this.fleet[this.activeShip]
            var c = 0;
            if( this.game.stateOrientation === "horizontal") {
                for(var i = 0; i < Math.round((shipLength/2)); i++) {
                    if(this.grid[posY][posX + i] === 0)
                        c++;                           
                    if(this.grid[posY][posX - i] === 0)
                        c++
                }
                if(shipLength % 2 === 0) {
                    if(this.grid[posY][posX - Math.round(ship.getLife()/2)] === 0) {
                        c++;
                    }
                }
            }
            else {
                for(var i = 0; i < Math.round((shipLength/2)); i++) {
                    if(this.grid[posY + i][posX] === 0)
                        c++;                       
                    if(this.grid[posY - i][posX] === 0)
                        c++
                }
                if(shipLength % 2 === 0) {
                    if(this.grid[posY + Math.round(ship.getLife()/2)][posX] === 0) {
                        c++;
                    }
                }
            }
            
            if(c - 1 === shipLength)
                return true;
        },
        setActiveShipPosition: function (x, y) {
            var shipLength = this.fleet[this.activeShip].life
            var ship = this.fleet[this.activeShip];
            var i = 0;
            if ( this.game.stateOrientation === "horizontal") {
                while (i < Math.round(ship.getLife()/2)) {
                    this.grid[y][x + i] = ship.getId();
                    this.grid[y][x - i] = ship.getId();
                    i += 1;
                }
                if(shipLength % 2 === 0) {
                    this.grid[y][x - Math.round(ship.getLife()/2)] = ship.getId()
                }
                this.game.stateOrientation = "horizontal"
                return true;
            } 
            else {
                while (i < Math.round(ship.getLife()/2)) {
                    this.grid[y  + i][x] = ship.getId();
                    this.grid[y  - i][x] = ship.getId();
                    i += 1;
                }
                if(shipLength % 2 ===0) {
                    this.grid[y + Math.round(ship.getLife()/2)][x] = ship.getId()
                }
                this.game.stateOrientation = "horizontal"
                return true;
            }
            
             
        },
        clearPreview: function () {
            this.fleet.forEach(function (ship) {
                if (ship.dom.parentNode) {
                    ship.dom.parentNode.removeChild(ship.dom);
                }
            });
        },
        resetShipPlacement: function () {
            this.clearPreview();

            this.activeShip = 0;
            this.grid = utils.createGrid(10, 10);
        },
        activateNextShip: function () {
            if (this.activeShip < this.fleet.length - 1) {
                this.activeShip += 1;
                return true;
            } else {
                return false;
            }
        },
        renderTries: function (grid) {
            this.tries.forEach(function (row, rid) {
                row.forEach(function (val, col) {
                    var node = grid.querySelector('.row:nth-child(' + (rid + 1) + ') .cell:nth-child(' + (col + 1) + ')');
                    if (val === true) {
                        node.style.backgroundColor = '#e60019';
                    } else if (val === false) {
                        node.style.backgroundColor = '#aeaeae';
                    }
                });
            });
        },
        renderShips: function (grid) {
        }
    };

    global.player = player;

}(this));