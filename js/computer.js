/*jslint browser this */
/*global _, player */

(function (global) {
    "use strict";

    var computer = _.assign({}, player, {
        grid: [],
        tries: [],
        fleet: [],
        currentPlayer: 1,
        game: null,
        play: function () {
            var self = this;
            var randomPosition = this.RandomShipPosition(0, 9);
            setTimeout(function () {
                self.game.fire(this, randomPosition[0], randomPosition[1], function (hasSucced) {
                    self.tries[randomPosition[1]][randomPosition[0]] = hasSucced;
                });
            }, 2000);
        },
        isShipOk: function (callback) {
            var i = 0;
            while(i < 4) {
                var cord = this.RandomShipPosition(2, 8)
                this.game.stateOrientation = cord[2] === 0 ? "horizontal" : "vertical";
                if (this.ConfirmShipPosition(cord[0], cord[1]) && this.CheckEmptyCase(cord[0], cord[1])) {
                    if(this.setActiveShipPosition(cord[0], cord[1])){
                        this.activateNextShip();
                        i++;
                    }
                }
            }
                setTimeout(function () {
                    callback();
                }, 500);
        },
        RandomShipPosition: function(min, max) {
            var position = [];
            for (var i = 0; i<2; i++){
                position[i] = Math.floor(Math.random() * Math.floor(max-min)+min)
            }
            position[2] = Math.floor(Math.random() * Math.floor(2 - 0))
            return position;
            
        }
    });

    global.computer = computer;

}(this));