/*jslint browser this */
/*global _, player, computer, utils */
var fire = new Image();
(function () {
    "use strict";

    var game = {
        PHASE_INIT_PLAYER: "PHASE_INIT_PLAYER",
        PHASE_INIT_OPPONENT: "PHASE_INIT_OPPONENT",
        PHASE_PLAY_PLAYER: "PHASE_PLAY_PLAYER",
        PHASE_PLAY_OPPONENT: "PHASE_PLAY_OPPONENT",
        PHASE_GAME_OVER: "PHASE_GAME_OVER",
        PHASE_WAITING: "waiting",

        currentPhase: "",
        phaseOrder: [],
        whoStart: 1,
        // garde une référence vers l'indice du tableau phaseOrder qui correspond à la phase de jeu pour le joueur humain
        playerTurnPhaseIndex: 2,

        // l'interface utilisateur doit-elle être bloquée ?
        waiting: false,

        // garde une référence vers les noeuds correspondant du dom
        grid: null,
        miniGrid: null,

        // liste des joueurs
        players: [],
        stateOrientation: "horizontal",

        // lancement du jeu
        init: function () {

            // initialisation
            this.grid = document.querySelector('.board .main-grid');
            this.miniGrid = document.querySelector('.board .mini-grid');

            // défini l'ordre des phase de jeu
            this.phaseOrder = [
                this.PHASE_INIT_PLAYER,
                this.PHASE_INIT_OPPONENT,
                this.PHASE_PLAY_PLAYER,
                this.PHASE_PLAY_OPPONENT,
                this.PHASE_GAME_OVER
            ];
            this.playerTurnPhaseIndex = 0;

            // initialise les joueurs
            this.setupPlayers();

            // ajoute les écouteur d'événement sur la grille
            this.addListeners();

            // c'est parti !
            this.goNextPhase();
        },
        setupPlayers: function () {
            // donne aux objets player et computer une réference vers l'objet game
            player.setGame(this);
            computer.setGame(this);

            // todo : implémenter le jeu en réseaux
            this.players = [player, computer];

            this.players[0].init();
            this.players[1].init();
        },
        goNextPhase: function () {
            // récupération du numéro d'index de la phase courante
            var ci = this.phaseOrder.indexOf(this.currentPhase);
            var self = this;

            if (ci < this.phaseOrder.length - 1) {
                this.currentPhase = this.phaseOrder[ci + 1];
            } else {
                this.currentPhase = this.phaseOrder[4];
            }

            switch (this.currentPhase) {
            case this.PHASE_GAME_OVER:
                // detection de la fin de partie
                if (!this.gameIsOver()) {
                    // le jeu n'est pas terminé on recommence un tour de jeu
                    this.currentPhase = this.phaseOrder[1];
                    this.goNextPhase();
                }
                break;
            case this.PHASE_INIT_PLAYER:
                utils.info("Placez vos bateaux");
                break;
            case this.PHASE_INIT_OPPONENT:
                this.wait();
                utils.info("En attente de votre adversaire");
                this.currentPhase = this.phaseOrder[this.whoStart];
               this.players[1].isShipOk(function () {
                    self.stopWaiting();
                    self.goNextPhase();
                });
                break;
            case this.PHASE_PLAY_PLAYER:
                utils.info("A vous de jouer, choisissez une case !");
                break;
            case this.PHASE_PLAY_OPPONENT:
                utils.info("A votre adversaire de jouer...");
                this.players[1].play();
                break;
            }
        },
        gameIsOver: function () {
            if(this.players[0].gameOver === 17) {
                utils.info("GameOver");
                var audio = new Audio('./mp3/gameover.mp3');
                audio.play();
                return true;
            } else if (this.players[1].gameOver === 17) {
                utils.info("Win");
                var audio = new Audio('./mp3/gagner.mp3');
                audio.play();
                return true;
            }
            return false;
        },
        getPhase: function () {
            if (this.waiting) {
                return this.PHASE_WAITING;
            }
            return this.currentPhase;
        },
        // met le jeu en mode "attente" (les actions joueurs ne doivent pas être pris en compte si le jeu est dans ce mode)
        wait: function () {
            this.waiting = true;
        },
        // met fin au mode mode "attente"
        stopWaiting: function () {
            this.waiting = false;
        },
        addListeners: function () {
            // on ajoute des acouteur uniquement sur la grid (délégation d'événement)
            this.grid.addEventListener('mousemove', _.bind(this.handleMouseMove, this));
            this.grid.addEventListener('click', _.bind(this.handleClick, this));
            this.grid.addEventListener('contextmenu', _.bind(this.contextClick, this));
        },
        handleMouseMove: function (e) {
            // on est dans la phase de placement des bateau
            if (this.getPhase() === this.PHASE_INIT_PLAYER && e.target.classList.contains('cell')) {
                var ship = this.players[0].fleet[this.players[0].activeShip];
                // si on a pas encore affiché (ajouté aux DOM) ce bateau
                if (!ship.dom.parentNode) {
                    this.grid.appendChild(ship.dom);
                    // passage en arrière plan pour ne pas empêcher la capture des événements sur les cellules de la grille
                    ship.dom.style.zIndex = -1;
                }
                // décalage visuelle, le point d'ancrage du curseur est au milieu du bateau
                if(ship.getName() === "Submarine" && this.stateOrientation == 'vertical') {
                    ship.dom.style.top = "" + (utils.eq(e.target.parentNode)) * utils.CELL_SIZE - (600 + (this.players[0].activeShip) * 60) + 30 +"px";
                    ship.dom.style.left = "" + utils.eq(e.target) * utils.CELL_SIZE - Math.floor(ship.getLife() / 2) * utils.CELL_SIZE + 30 + "px";
                }else {
                    ship.dom.style.top = "" + (utils.eq(e.target.parentNode)) * utils.CELL_SIZE - (600 + this.players[0].activeShip * 60) + "px";
                    ship.dom.style.left = "" + utils.eq(e.target) * utils.CELL_SIZE - Math.floor(ship.getLife() / 2) * utils.CELL_SIZE + "px";
                }
            }
        },
        handleClick: function (e) {
            // self garde une référence vers "this" en cas de changement de scope
            var self = this;
            // si on a cliqué sur une cellule (délégation d'événement)
            if (e.target.classList.contains('cell')) {
                // si on est dans la phase de placement des bateau
                if (this.getPhase() === this.PHASE_INIT_PLAYER) {
                    if(player.ConfirmShipPosition(utils.eq(e.target), utils.eq(e.target.parentNode)) && player.CheckEmptyCase(utils.eq(e.target), utils.eq(e.target.parentNode))){ 
                        // on enregistre la position du bateau, si cela se passe bien (la fonction renvoie true) on continue
                        if (this.players[0].setActiveShipPosition(utils.eq(e.target), utils.eq(e.target.parentNode))) {
                            // et on passe au bateau suivant (si il n'y en plus la fonction retournera false)
                            if (!this.players[0].activateNextShip()) {
                                this.wait();
                                utils.confirm("Confirmez le placement ?", function () {
                                    // si le placement est confirmé
                                    self.stopWaiting();
                                    self.renderMiniMap();
                                    self.players[0].clearPreview();
                                    var inputs = document.getElementsByName('whoStart');
                                    var length = inputs.length;
                                    for (var i = 0; i < length; i+=1) {
                                        if(inputs[i].checked) {
                                            if(inputs[i].value == 3) {
                                                    self.whoStart = Math.floor(Math.random() * Math.floor(3 - 1) + 1);    
                                                }
                                                else {
                                                    self.whoStart = inputs[i].value;
                                                }
                                            break;
                                        }
                                    }
                                    self.goNextPhase();
                                }, function () {
                                    self.stopWaiting();
                                    // sinon, on efface les bateaux (les positions enregistrées), et on recommence
                                    self.players[0].resetShipPlacement();
                                });
                            }
                        }
                    }
                // si on est dans la phase de jeu (du joueur humain)
                } else if (this.getPhase() === this.PHASE_PLAY_PLAYER){
                        this.players[0].play(utils.eq(e.target), utils.eq(e.target.parentNode));
                }
            }
        },
        contextClick : function (e) {
            e.preventDefault();
            var ship = this.players[0].fleet[this.players[0].activeShip];

            if(this.stateOrientation === "horizontal") {
                this.stateOrientation = "vertical"
                ship.dom.style.transform = 'rotate(90deg)';
            } else {
                this.stateOrientation = "horizontal"
                ship.dom.style.transform = 'rotate(0deg)';
            }
        },
        // fonction utlisée par les objets représentant les joueurs (ordinateur ou non)
        // pour placer un tir et obtenir de l'adversaire l'information de réusssite ou non du tir
        fire: function (from, col, line, callback) {
            this.wait();
            var self = this;
            var msg = "";
            // determine qui est l'attaquant et qui est attaqué
            
            var target = this.players.indexOf(from) === 0
            ? this.players[1]
            : this.players[0];
            
            if (this.currentPhase === this.PHASE_PLAY_OPPONENT) {
                msg += "Votre adversaire vous a... ";
            }
            var audio = new Audio('./mp3/tir.mp3');
            audio.play();
            setTimeout(function () {
                // on demande à l'attaqué si il a un bateaux à la position visée
                // le résultat devra être passé en paramètre à la fonction de callback (3e paramètre)
                target.receiveAttack(col, line, function (hasSucceed) {
                    
                    if (hasSucceed) {
                        if(target.currentPlayer === 1) {
                            if(document.querySelectorAll('.main-grid>.row:nth-of-type('+(line+1)+')>.cell:nth-of-type('+(col+1)+')')[0].style.backgroundColor === 'red' && target.grid[line][col] === 10){
                                msg += "Encore ";
                            }
                            else {
                                target.gameOver += 1;
                            }
                            self.createAnimation(col, line, "./mp3/anim.gif");
                            document.querySelectorAll('.main-grid>.row:nth-of-type('+(line+1)+')>.cell:nth-of-type('+(col+1)+')')[0].style.backgroundColor = 'red';
                        }
                        else {
                            if(document.querySelectorAll('.mini-grid>.row:nth-of-type('+(line+1)+')>.cell:nth-of-type('+(col+1)+')')[0].style.backgroundColor === 'red') {
                                msg += "Encore ";
                            }
                            else {
                                document.querySelectorAll('.mini-grid>.row:nth-of-type('+(line+1)+')>.cell:nth-of-type('+(col+1)+')')[0].style.backgroundColor = 'red';
                                player.fleet[target.touchedBoat - 1].life -= 1;
                                if(player.fleet[target.touchedBoat - 1].life === 0) {
                                    document.querySelectorAll('.fleet>*')[target.touchedBoat - 1].className = " sunk";
                                 }
                            }
                        }
                        msg += "Touché !";
                        var audio = new Audio('./mp3/touche.mp3');
                        audio.play();
                        
                    } else {
                        
                        if(target.currentPlayer === 1) {
                            if(document.querySelectorAll('.main-grid>.row:nth-of-type('+(line+1)+')>.cell:nth-of-type('+(col+1)+')')[0].style.backgroundColor === 'grey') {
                                msg += "Encore ";
                            }
                            document.querySelectorAll('.main-grid>.row:nth-of-type('+(line+1)+')>.cell:nth-of-type('+(col+1)+')')[0].style.backgroundColor = 'grey';
                            self.createAnimation(col, line, "./mp3/fish.gif");
                            
                        }
                        else {
                            if(document.querySelectorAll('.mini-grid>.row:nth-of-type('+(line+1)+')>.cell:nth-of-type('+(col+1)+')')[0].style.backgroundColor === 'grey') {
                                msg += "Encore ";
                            }
                        }
                        msg += "Manqué...";
                        var audio = new Audio('./mp3/chevre.mp3');
                        audio.play();
                    }
                    utils.info(msg);
                    // on invoque la fonction callback (4e paramètre passé à la méthode fire)
                    // pour transmettre à l'attaquant le résultat de l'attaque
                    callback(hasSucceed);
    
                    // on fait une petite pause avant de continuer...
                    // histoire de laisser le temps au joueur de lire les message affiché
                    setTimeout(function () {
                        self.stopWaiting();
                        self.goNextPhase();
                    }, 1000);
                });
                
            }, 2000);
            
        },
        createAnimation: function(col, line, srcs) {
            var canvas = document.createElement('canvas');
            var context = canvas.getContext('2d');
            
            fire.src = srcs;
            //context.drawImage(fire, 0, 0, 60, 60,  0, 0, 60, 60);
            canvas.setAttribute('id','canvas');
            var cell =  document.querySelectorAll('.main-grid>.row:nth-of-type('+(line+1)+')>.cell:nth-of-type('+(col+1)+')')[0];
            cell.insertBefore(canvas, null);
            var i = 1; var j = 0;
            var timer = setInterval(function(){
                context.drawImage(fire, 64*(i-1), 64*(j-1), 60, 60,  0, 0, 60, 60);
                if(i%5===0) {
                    i=1;
                    j++;
                } else{
                    i++;
                }
                if(i >= 5 && j >= 5) {
                    document.getElementById("canvas").remove();
                    clearInterval(timer);
                }
            },50);
        },
        renderMap: function () {
            this.players[0].renderTries(this.grid);
        },
        renderMiniMap: function () {
            document.getElementsByClassName("mini-grid")[0].innerHTML = document.getElementsByClassName('main-grid')[0].innerHTML;
            document.getElementsByClassName("mini-grid")[0].style.marginTop = "-200px";
        }
    };
    // point d'entrée
    document.addEventListener('DOMContentLoaded', function () {
        game.init();
    });

}());